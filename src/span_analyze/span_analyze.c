/* file byte span analyzer */

#include <stdio.h>
#include <stdlib.h>

#define MIN_FOR_EQU 3L

struct node {
	long span_length;
	long qty;
	struct node *next;
};

int analyze_file(FILE *fp_in);
struct node *node_update(struct node *list, long equal_bytes);

int main(int argc, char *argv[])
{
	FILE *fp_in;

	if (argc != 2) {
		fprintf(stderr, "counts spans of repeated bytes in a file\n"
		                "usage: span_analyze filename\n");
		return -1;
	}

	fp_in = fopen(argv[1], "rb");
	if (fp_in == NULL) {
		fprintf(stderr, "Error opening file %s\n", argv[1]);
		return -2;
	}

	analyze_file(fp_in);

	fclose(fp_in);
	return 0;
}

int analyze_file(FILE *fp_in)
{
	struct node *list_top = NULL;
	struct node *list_p;

	int current_byte;
	int previous_byte;
	long equal_bytes = 1L;

	current_byte = getc(fp_in);
	if (current_byte == EOF) {
		fprintf(stderr, "Empty file\n");
		return -3;
	}
	previous_byte = current_byte;

	for (;;) {
		current_byte = getc(fp_in);

		if (current_byte == previous_byte) {
			++equal_bytes;
			previous_byte = current_byte;
			continue;
		}

		if (equal_bytes >= MIN_FOR_EQU) {
			list_top = node_update(list_top, equal_bytes);
		}

		if (current_byte == EOF) {
			break;
		}

		equal_bytes = 1;
		previous_byte = current_byte;
	}

	if (list_top != NULL) {
		for (list_p = list_top; list_p != NULL; list_p = list_p->next) {
			printf("Span of %ld: %ld\n", list_p->span_length, list_p->qty);
		}
	}
	else {
		printf("No spans of repeated bytes\n");
	}

	list_p = list_top;
	while (list_p != NULL) {
		list_p = list_p->next;
		free(list_top);
		list_top = list_p;
	}

	return 0;
}

struct node *node_update(struct node *list, long equal_bytes)
{
	struct node *new_node;
	struct node *q;
	struct node *trailing_q;

	// List is empty
	if (list == NULL) {
		new_node = malloc(sizeof(*new_node));
		if (new_node == NULL) {
			fprintf(stderr, "Error allocating memory for node\n");
			return NULL;
		}

		new_node->span_length = equal_bytes;
		new_node->qty = 1;
		new_node->next = NULL;

		return new_node;
	}

	q = list;

	// New node goes in the beginning of the list
	if (q->span_length > equal_bytes) {
		new_node = malloc(sizeof(*new_node));
		if (new_node == NULL) {
			fprintf(stderr, "Error allocating memory for node\n");
			return list;
		}

		new_node->span_length = equal_bytes;
		new_node->qty = 1;
		new_node->next = list;

		return new_node;
	}

	// Update existing node
	while (q != NULL && q->span_length < equal_bytes) {
		trailing_q = q;
		q = q->next;
	}
	if (q != NULL && q->span_length == equal_bytes) {
		++(q->qty);

		return list;
	}

	// New node
	new_node = malloc(sizeof(*new_node));
	if (new_node == NULL) {
		fprintf(stderr, "Error allocating memory for node\n");
		return list;
	}

	new_node->span_length = equal_bytes;
	new_node->qty = 1;
	new_node->next = q;
	trailing_q->next = new_node;

	return list;
}