/* Checks if two files are equal at the byte level */

#include <stdio.h>

#define BUFFER_SIZE 1024

int check_equal(FILE *a, FILE *b);

int main(int argc, char *argv[])
{
	if (argc != 3) {
		fprintf(stderr, "Checks if two files are equal at the byte level\n"
		                "usage: areequal file1 file2\n");
		return -1;
	}

	FILE *fp_a;
	FILE *fp_b;

	if ((fp_a = fopen(argv[1], "rb")) == NULL) {
		fprintf(stderr, "Could not open file %s\n", argv[1]);
		return -2;
	}
	if ((fp_b = fopen(argv[2], "rb")) == NULL) {
		fprintf(stderr, "Could not open file %s\n", argv[2]);
		fclose(fp_a);
		return -3;
	}

	static const char *msg[] = {"Files are not equal\n",
	                            "Files are equal\n",
	                            "Error\n"};        // Should never be printed
	int result;

	result = check_equal(fp_a, fp_b);
	if (result != 0 && result != 1)  result = 2;   // Should never happen
	printf("%s", msg[result]);

	if (ferror(fp_a))  printf("There were errors reading file %s\n", argv[1]);
	if (ferror(fp_b))  printf("There were errors reading file %s\n", argv[2]);

	fclose(fp_a);
	fclose(fp_b);

	return result;
}

int check_equal(FILE *a, FILE *b)
{
	unsigned char buffer_a[BUFFER_SIZE];
	unsigned char buffer_b[BUFFER_SIZE];
	size_t elems_a = 0;
	size_t elems_b = 0;
	size_t i;

	do {
		elems_a = fread(buffer_a, 1, BUFFER_SIZE, a);
		elems_b = fread(buffer_b, 1, BUFFER_SIZE, b);
		if (elems_a != elems_b) {
			return 0;                                 // First difference detected is different number of bytes
		}
		for (i = 0; i < elems_a; i++) {
			if (buffer_a[i] != buffer_b[i]) {
				return 0;                             // First difference detected is different bytes
			}
		}
	} while (feof(a) == 0 && feof(b) == 0);

	return 1;                                         // Difference tests fail, files are equal
}