/* Simple hexadecimal file inspector */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define ROW_SIZE 10

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Simple hex file viewer\n");
		fprintf(stderr, "usage: hexview filename [byte_limit]\n");
		exit(EXIT_FAILURE);
	}

	FILE *fp;
	int byte_array[ROW_SIZE];  // Stores read bytes and EOFs
	int line = 0;              // The offset in the file of each printed line
	int max_offset = 0;
	int i;

	if ((fp = fopen(argv[1], "rb")) == NULL) {
		fprintf(stderr, "could not open file %s\n", argv[1]);
		exit(EXIT_FAILURE);
	}
	if (argc == 3) {
		max_offset = atoi(argv[2]);
	}

	printf("Offset             Bytes               Characters\n");
	printf("------  -----------------------------  ----------\n");

	for (;;) {
		if ((byte_array[0] = getc(fp)) == EOF) {
			break;
		}
		for (i = 1; i < ROW_SIZE; i++) {
			byte_array[i] = getc(fp);
		}

		printf("%6d  ", line);

		for (i = 0; i < ROW_SIZE; i++) {
			if (byte_array[i] != EOF) {
				printf("%.2X ", byte_array[i]);
			}
			else {
				printf("   ");
			}
		}
		printf(" ");

		for (i = 0; i < ROW_SIZE; i++) {
			if (byte_array[i] != EOF) {
				if (isprint(byte_array[i])) {
					printf("%c", byte_array[i]);
				}
				else {
					printf(".");
				}
			}
			else {
				printf(" ");
			}
		}

		printf("\n");

		if (max_offset != 0 && line >= max_offset) {
			break;
		}
		line += ROW_SIZE;
	}

	fclose(fp);

	return 0;
}
